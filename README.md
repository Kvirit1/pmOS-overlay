This repo hosts some files that I use for PostMarketOS on the Nokia N900.

Software Dependencies:
 - xbindkeys

What you shoud run:

run scripts/init.sh as root
  - gives screen on/off ability
  - disables charge led (annoying)
  
Place .xbindkeysrc in your /home/user directory
  - Run xbindkeys -f .xbindkeyssrc
    - Gives volume control using volume buttons
    - Dim screen on/off using screen slider
  